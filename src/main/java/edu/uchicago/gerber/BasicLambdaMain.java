package edu.uchicago.gerber;

import jdk.nashorn.internal.objects.Global;

import java.io.File;
import java.io.FileFilter;

import java.util.*;

/**
 * Created by ag on 8/15/14.
 */
public class BasicLambdaMain {


    public static void main(String[] args){

        //from main thread
        System.out.println("from main");


        //has a reference, but still anonymous
        Runnable runnable = new Runnable() {

            @Override
            public void run() {

                System.out.println("HelloWorld from thread"
                        + Thread.currentThread().getName());
            }
        };



         Runnable runnableL = () ->   {

             System.out.println("HelloWorld from thread"
                     + Thread.currentThread().getName());
         };





        new Thread(runnableL).start();

        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().endsWith(".pdf");
            }
        };


        FileFilter filterL = (final File file) ->  file.getName().endsWith(".pdf");

        //without type
        FileFilter filterL2 =  file ->  file.getName().endsWith(".pdf");

        File dir = new File("c:/dev");     //change this dir so that it makes sense on your op system
        File[] pdfFiles = dir.listFiles(filterL2);
        for (File f: pdfFiles){
            System.out.println(f);
        }



        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
               return Integer.compare(s1.length(), s2.length());
            }
        };



        Comparator<String> compL =  (final String s1, final String s2) ->   Integer.compare(s1.length(), s2.length());

        //without types
        Comparator<String> compL2 =  ( s1, s2) ->   Integer.compare(s1.length(), s2.length());



        List<String> list = Arrays.asList("***", "**", "****", "*");
        Collections.sort(list, compL2);
        for (String s : list) {
            System.out.println(s);
        }



    }



}


