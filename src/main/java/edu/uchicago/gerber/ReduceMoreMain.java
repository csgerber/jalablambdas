package edu.uchicago.gerber;

import java.util.*;

/**
 * Created by ag on 8/15/14.
 */
public class ReduceMoreMain {

    public static void main(String[] args) {
        final List<Integer> nums =

                //you can see the identity as well
             //   new ArrayList<Integer>();
                //or uncomment this.
              //  Arrays.asList(1,2,3, 45,67);
              Arrays.asList(-1,-2,-3, -45,-67);

        System.out.println(
                nums.stream()
                        //this is a binaryOperator and it folds along the entire stream
                        .reduce(0, (a1, a2) -> a1 + a2));


        //take a look at the javadocs for Integer:sum
        System.out.println(
                nums.stream()
                        //this is a binaryOperator and it folds along the entire stream
                        .reduce(0, Integer::sum));


        //take a look at the javadocs for Integer:sum

        Optional<Integer> optional = nums.stream()
                //this is a binaryOperator and it folds along the entire stream
                .reduce(Integer::max);

        System.out.println(optional.isPresent() ? optional.get() : "empty" );





    }   //end main






}
