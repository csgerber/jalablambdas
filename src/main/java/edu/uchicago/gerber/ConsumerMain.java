package edu.uchicago.gerber;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by ag on 8/15/14.
 */
public class ConsumerMain {

    public static void main(String[] args) {

        Stream<String> stringStream = Stream.of("Adam", "Gregg", "Kevin", "Kirk", "Vanessa");
        stringStream
                .forEach(stringConsumer);

        //need to reload the stream as the iterator is at the end
        stringStream = Stream.of("Adam", "Gregg", "Kevin", "Kirk", "Vanessa");

        stringStream
                .peek(name -> System.out.print( name + " : "))
                .map(convertToLen)           //converted from Stream<String> to Stream<Integer>
                .forEach(System.out::println);


    }

    static Consumer<String> stringConsumer = new Consumer<String>() {
        @Override
        public void accept(String s) {
            //do something with each element of a stream
            System.out.println("my name is" + s);


        }
    };

    static Function<String, Integer> convertToLen = new Function<String, Integer>() {
        @Override
        public Integer apply(String s) {
            return s.length();
        }
    };
}
