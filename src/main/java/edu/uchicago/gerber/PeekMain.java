package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ag on 8/15/14.
 */
public class PeekMain {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Chicago", "Toronto", "Boston", "Vancouver", "New York");

        List<String> results = new ArrayList<String>();


        //map-filter-reduce
        //filter-map-reduce

        list.stream()
               // .peek(System.out::println)  //itermediary   - returns a Stream   lazy
                .peek(results::add)
                .filter(s -> s.endsWith("o"))    //itermediary    - returns a Stream     lazy
                  //this is inferring the element, takes a consumer to do some work
                .forEachOrdered(System.out::println);    //terminal - returns void        eager


        System.out.println("the size of the results " + results.size());

    }
}
