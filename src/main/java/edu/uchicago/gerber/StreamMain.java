package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by ag on 8/15/14.
 */
public class StreamMain {

    //map-filter-reduce pattern for streams

    //map is a transform operation. It takes one type and returns another type, or it takes one type and returns a modified same type
    //filter shortens the list to filter criteria
    //reduce is similar to a SQL aggregation like "sum", "max", "min", "avg".

    //a stream is NOT a collection and it does not hold any data
    //a stream is most like an Iterator
    //a stream should NOT change the data it processes, so that it allows  concurrency. Do not modify the data in a stream
    //a stream processes data in one pass


    public static void main(String[] args) {

        Stream<String> streamString = Stream.of("one", "two", "three", "four", "five");

        Predicate<String> p1 = s -> s.length() > 3;
        Predicate<String> p2 = s -> s.equalsIgnoreCase("two");
        Predicate<String> p3 = s -> s.equalsIgnoreCase("three");


        streamString
                .filter(p1.and(p3))
                .forEach(s -> System.out.println(s));


        //keep in mind that a stream can not hold any data and if you call it again, it will throw an exception  unless you uncomment this code
        // streamString = Stream.of("one", "two", "three", "four", "five");
        streamString
                .filter(p1.and(p3))
                .forEach(s -> System.out.println(s));





//        List<String> result = new ArrayList<>();
//
//        List<Person> persons = new ArrayList<Person>();
//        persons.add(new Person("James", 16));
//        persons.add(new Person("Jonny", 14));
//        persons.add(new Person("Katey", 15));
//        persons.add(new Person("Holy", 17));
//
//
//        Consumer<String> c1 = s -> result.add(s);
//        Consumer<String> c2 = s -> System.out.println(s);


    }


}
