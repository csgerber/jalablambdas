package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by ag on 8/15/14.
 */
public class ForEachMain {


    public static void main(String[] args) {
        List<String> list = Arrays.asList("Chicago", "Toronto", "Boston", "Vancouver", "New York");
        List<String> results = new ArrayList<String>();

        list.forEach(city -> System.out.println(city));
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        list.forEach(System.out::println);




        Consumer<String> c1 =  s -> System.out.println(s);
        Consumer<String> c2 =   System.out::println;
        Consumer<String> c3 =    s -> results.add(s);
        Consumer<String> c4 =    results::add;
        Consumer<String> c5 =    s -> results.add("city : " +s);



        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        list.forEach(c2.andThen(c4).andThen(c5));
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        System.out.println(results.size());
        results.forEach(c2);




        //forEach is available on any collection in Java8 and it takes a Consumer
        //it's defined as a default method in the Iterable interface in Java8. The default keyword makes this method
        //backwards compatible and does not break any existing code
        //we can now add implemented methods to an interface in Java8












    }



}
