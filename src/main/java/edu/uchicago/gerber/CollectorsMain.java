package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by ag on 8/15/14.
 */
public class CollectorsMain {


    public static void main(String[] args) {
        List<Person> peeps = new ArrayList<Person>();


        peeps.add(new Person("Emily", 12));
        peeps.add(new Person("Natey", 21));
        peeps.add(new Person("Jimmy", 45));
        peeps.add(new Person("Holly", 41));
        peeps.add(new Person("Buffy", 15));


        Map<Integer, List<Person>> result =
                peeps.stream()
                .filter(person -> person.getAge() > 30)  //itermediary operation, Stream<Person>
                .collect(Collectors.groupingBy(Person::getAge));

        for (Integer integer : result.keySet()) {
            System.out.println(integer + result.get(integer).toString());
        }


    }


}
