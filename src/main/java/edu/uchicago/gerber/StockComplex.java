package edu.uchicago.gerber;

/**
 * Created by ag on 8/15/14.
 */
public class StockComplex {

    private String ticker;
    private String date;
    private String open;
    private String high;
    private String low;
    private String close;
    private String volume;
    private String adjclose;



    public StockComplex(String ticker, String date, String open, String high, String low, String close, String volume, String adjclose) {
        this.ticker = ticker;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.adjclose = adjclose;
    }

    @Override
    public String toString() {
        return "StockComplex{" +
                "ticker='" + ticker + '\'' +
                ", date='" + date + '\'' +
                ", open='" + open + '\'' +
                ", high='" + high + '\'' +
                ", low='" + low + '\'' +
                ", close='" + close + '\'' +
                ", volume='" + volume + '\'' +
                ", adjclose='" + adjclose + '\'' +
                '}';
    }
}
