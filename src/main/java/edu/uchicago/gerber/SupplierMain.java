package edu.uchicago.gerber;

import java.util.function.Supplier;

/**
 * Created by ag on 8/15/14.
 */
//taken from http://java.dzone.com/articles/supplier-interface
public class SupplierMain {

    public static void main(String[] args) {
        driveVehicle(() -> new Vehicle());
        driveVehicle(() -> new Car());

        driveVehicle(Vehicle::new);
        driveVehicle(Car::new);

        driveVehicle(supplierVehicle);
        driveVehicle(supplierCar);



        //we can not do this
       // driveVehicle((Supplier<? extends Vehicle>) new Vehicle());
       // driveVehicle((Supplier<? extends Vehicle>) new Car());
    }

    //this promotes late (run-time) binding
    private static void driveVehicle(Supplier<? extends Vehicle> supplier) {

        Vehicle vehicle = supplier.get();
        vehicle.drive();

    }

   static Supplier<Car> supplierCar = new Supplier<Car>() {
        @Override
        public Car get() {
            return new Car();
        }
    };

    static Supplier<Vehicle> supplierVehicle = new Supplier<Vehicle>() {
        @Override
        public Vehicle get() {
            return new Vehicle();
        }
    };


}

class Vehicle {

    public void drive() {

        System.out.println("Driving vehicle ...");

    }

}

class Car extends Vehicle {

    @Override

    public void drive() {

        System.out.println("Driving car...");

    }

}
