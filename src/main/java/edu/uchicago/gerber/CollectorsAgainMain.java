package edu.uchicago.gerber;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ag on 8/15/14.
 */

//example taken loosely from pluralsight tutorial on Java8 by Jose Paumard
public class CollectorsAgainMain {


    public static void main(String[] args) {
        List<Person> persons = new ArrayList<Person>();

        String workingDir = System.getProperty("user.dir");
        //try with resources
        try (
                BufferedReader br = new BufferedReader(new FileReader(workingDir + "/persons.txt"));
                Stream<String> stream = br.lines();
        ) {


            //convert a Stream<String> to a Stream<Person>
            stream.map(line -> {
                String[] s = line.split(" ");
                Person p = new Person(s[0].trim(), Integer.parseInt(s[1]));
                persons.add(p);
                return p;

            }).forEach(System.out::println);


        } catch (Exception e) {
            e.printStackTrace();
        }


        Optional<Person> opt = persons.stream()
                .filter(p -> p.getAge() >= 20)
                //.min takes a Comparator<Person> and comparing returns a Comparator<Person>
                .min(Comparator.comparing(Person::getAge));

        System.out.println(opt);

      // Stream<Person> streamPeeps = person



    }


}
