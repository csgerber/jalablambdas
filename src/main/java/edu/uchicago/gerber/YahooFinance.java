package edu.uchicago.gerber;

import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


/**
 * Created by ag on 8/15/14.
 */

public class YahooFinance {


    public static void main(String[] args) {


        String workingDir = System.getProperty("user.dir");


        String strConifFilePath = workingDir + "/output" + System.currentTimeMillis() + ".txt";
        File file = new File(strConifFilePath);
        if (!file.exists()){
            try {
                //will only create one if it doesn't already exist
                file.createNewFile();
            } catch (IOException ex) {
                return;
            }
        }



        //try with resources
        try (
                BufferedReader br = new BufferedReader(new FileReader(workingDir + "/tickers.txt"));
                FileOutputStream   output = new FileOutputStream(file);
                Stream<String> streamFromFile = br.lines();
        ) {


            System.out.println(
                    streamFromFile
                            //run this in parellel (concurrently)
                            .parallel()
                            .filter(ticker -> ticker.startsWith("G"))
                            .peek(System.out::println)    //this will just print "GE" or whatever stocks you have filtered out
                                    //this will print to a file  and to the console
                            .peek(ticker -> printStringStream(ticker, output))
                            .count()); //force a terminal operation


        } catch (Exception e) {
            e.printStackTrace();
       }

    }



    public static void printStringStream(final String ticker, final FileOutputStream outputStream) {
        try {
            final URL url =
                    new URL("http://ichart.finance.yahoo.com/table.csv?s=" + ticker);
            final BufferedReader reader =
                    new BufferedReader(new InputStreamReader(url.openStream()));

            Stream<String>  stream = reader.lines();
            stream
                    //filter out the header line
                    .filter(line -> !line.startsWith("Date"))
                    .map(line -> {
                        final String[] dataItems = line.split(",");

                        StockComplex stockComplex = new StockComplex(ticker, dataItems[0], dataItems[1], dataItems[2], dataItems[3], dataItems[4], dataItems[5], dataItems[6]);
                        try {
                            outputStream.write(stockComplex.toString().getBytes(Charset.forName("UTF-8")));
                            outputStream.write("\n".getBytes(Charset.forName("UTF-8")));

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return stockComplex;


                    })
                      .forEach(System.out::println);

        } catch(Exception ex) {
            // throw new RuntimeException(ex);
           // return null;        e.printStackTrace();
            ex.printStackTrace();
        }
    }



}

