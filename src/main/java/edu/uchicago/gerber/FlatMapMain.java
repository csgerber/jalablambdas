package edu.uchicago.gerber;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by ag on 8/15/14.
 */
public class FlatMapMain {

    public static void main(String[] args) {
        List<Integer> list1 = Arrays.asList(1,2,3,4,5,6);
        List<Integer> list2 = Arrays.asList(2,4,6);
        List<Integer> list3 = Arrays.asList(3,5,7);

        List<List<Integer>> aggregateLists = Arrays.asList(list1,list2,list3);
        System.out.println(aggregateLists);

        Function<List<?>, Integer>  sizeFunction = List::size;
        Function<List<Integer>, Stream<Integer>> flatMapperFunction = list -> list.stream();

        aggregateLists.stream()
                //.flatMap(flatMapperFunction)
                .map(sizeFunction)
                .forEach(System.out::println);



    }
}
