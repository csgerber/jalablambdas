package edu.uchicago.gerber;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by ag on 8/15/14.
 */
public class FriendsMain {

    public static void main(String[] args) {

        final List<String> friends =
                Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");


        //create the consumer anonymously
        System.out.println("%%%%%%%%%%%%%%create the consumer explicity%%%%%%%%%%%%%%");
        //the forEach method takes a Consumer
        friends.forEach(new Consumer<String>() {
            public void accept(final String name) {
                System.out.println(name);
            }
        });

        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(final String s) {
                System.out.println("name: " + s);

            }
        };


        //create a lambda
        System.out.println("%%%%%%%%%%%%%%%%%create a lambda %%%%%%%%%%%%%%");
        friends.forEach((final String name) -> System.out.println(name));
        System.out.println("%%%%%%%%%%%%%%%%%% infer  the return type %%%%%%%%%%%%%%%%");
        //you can infer the type (this will be final)
        friends.forEach(name -> System.out.println(name));
        System.out.println("%%%%%%%%%%%%%%%%%%% use method reference %%%%%%%%%%%%%%%%");
        //you can use a method reference as well
        friends.forEach(System.out::println);
        System.out.println("%%%%%%%%%%%%%%%%%%% pass a consumer %%%%%%%%%%%%%%%%");
        //you can use a method reference as well
        friends.forEach(consumer);



    }
}
