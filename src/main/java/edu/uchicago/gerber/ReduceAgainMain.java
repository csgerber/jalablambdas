package edu.uchicago.gerber;

import java.util.List;
import java.util.Map;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ag on 8/15/14.
 */
//based on http://docs.oracle.com/javase/tutorial/collections/streams/reduction.html
public class ReduceAgainMain {
    public static void main(String[] args) {


        Stream<String> stringStream = Stream.of("Katy", "Jonny", "Denny", "Bobby", "Danny", "Billy", "Holly");

        Integer totalAge =
                stringStream
                        .mapToInt(name -> name.length())
                                //sum is a terminal operation
                        .sum();

        //reload stream
        stringStream = Stream.of("Katy", "Jonny", "Denny", "Bobby", "Danny", "Billy", "Holly");

        Integer totalAgeReduce =
                stringStream
                        .map(name -> name.length())
                                //(param1 = identity, param2 = accumulator)
                                //this is the same as sum() above
                                //terminal operation that folds the values on top of one another accumulating, starting with zero
                        .reduce(0, (a, b) -> a + b);

        System.out.println(totalAge + ":" + totalAgeReduce);

        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


        //reload stream
        stringStream = Stream.of("Katy", "Jonny", "Denny", "Bobby", "Danny", "Billy", "Holly");

        Averager averageCollect = stringStream
                .filter(name -> name.length() >= 5)
                        //transform from Stream<String> to Stream<Integer>
                .map(name -> name.length())
                        //param1 = supplier, param2 = accumulator, param3 = combiner
                        //the supplier will create and return a new instance
                        //the accumulator will operate on each indvidual element
                        //the combiner will perform the aggregate calculation
                .collect(Averager::new, Averager::accept, Averager::combine);

        System.out.println("Average number of letters of a name in the list is: " +
                averageCollect.average());


        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


        //reload stream
        stringStream = Stream.of("Kai", "Bif", "Jonny", "Denny", "Bobby", "Danny", "Billy-Joe", "Holly");

        Map<Integer, List<String>> bySize =
                stringStream
                        //this is a terminal operation that will create a map of elements grouped by the number of their
                        .collect(Collectors.groupingBy(name -> name.length()));

        System.out.println(bySize);

        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


        //reload stream
        stringStream = Stream.of("Kai", "Bif", "Jonny", "Denny", "Bobby", "Danny", "Billy-Joe", "Holly");

        Map<Character, List<String>> namesByFirstLetter =
                stringStream
                        .collect(
                                Collectors.groupingBy(
                                        name -> name.charAt(0)));

        System.out.println(namesByFirstLetter);

        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        //reload stream
        stringStream = Stream.of("Kai", "Bif", "Jonny", "Jimmy-Bo", "Dennis", "Bobby", "Danny", "Billy-Joe", "Holly");

        Map<Character, Double> averageByFirstLetter =
                stringStream
                .collect(
                        Collectors.groupingBy(
                                name -> name.charAt(0),
                                Collectors.averagingInt(name -> name.length())));



        System.out.println(averageByFirstLetter);

    }  //end main

}

class Averager implements IntConsumer {
    private int total = 0;
    private int count = 0;

    public double average() {
        return count > 0 ? ((double) total) / count : 0;
    }

    public void accept(int i) {
        total += i;
        count++;
    }

    public void combine(Averager other) {
        total += other.total;
        count += other.count;
    }
}
