package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by ag on 8/15/14.
 */
public class IsPrimeMain {

    public static void main(String[] args) {

        final List<Integer> list = new ArrayList<Integer>();
        for (int nC = 0; nC < 5000; nC++) {
            list.add(nC);
        }

        //pass this into a .filter() operation
        Predicate<Integer> predicate = new Predicate<Integer>() {
            @Override
            public boolean test(Integer num) {
                //check if n is a multiple of 2
                if (num % 2 == 0) return false;
                //if not, then just check the odds
                for (int i = 3; i * i <= num; i += 2) {
                    if (num % i == 0)
                        return false;
                }
                return true;
            }
        };

        //pass this into a .map() operation
        Function<Integer,Integer> functionInvert = new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return -integer;
            }
        };



        list.stream()
                .filter(
                        num ->
                        {
                            //check if n is a multiple of 2
                            if (num % 2 == 0) return false;
                            //if not, then just check the odds
                            for (int i = 3; i * i <= num; i += 2) {
                                if (num % i == 0)
                                    return false;
                            }
                            return true;

                        }

                ).forEach(System.out::println);



        list.stream()
                .filter(predicate)
                .map(functionInvert)
                .sorted()
                .forEach(System.out::println);


    }//end


    //we won't use this, but we can use it as a reference.
    private static boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n % 2 == 0) return false;
        //if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }





}
