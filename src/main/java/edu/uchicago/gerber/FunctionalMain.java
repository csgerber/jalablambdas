package edu.uchicago.gerber;


//this is a new class
import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by ag on 8/15/14.
 */
public class FunctionalMain {

    //a consumer consumes an element of a stream
    //a stream is an iterator

    Consumer<String> c1 = s -> System.out.println(s);
    //we can also write this expression like so
    Consumer<String> c2 = System.out::println;


    Comparator<Integer> comp1 = (i1, i2) -> Integer.compare(i1, i2);
    Comparator<Integer> comp2 =  Integer::compare;



    Predicate<String> pSevenChars = s -> s.length() == 7;
    Predicate<String> pBeginsC = s -> s.startsWith("C");
    Predicate<String> pSevenC = pSevenChars.and(pBeginsC);




}
