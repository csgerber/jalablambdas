package edu.uchicago.gerber;

import java.util.*;
import java.util.function.Supplier;

/**
 * Created by ag on 8/15/14.
 */
public class ReduceMain {

    public static void main(String[] args) {
        final List<String> friends =
       //    new ArrayList<String>();
                Arrays.asList("Brian", "Natey", "Neally", "Raju", "Sara", "Scottey", "Holly");

        //redude is an SQL-like method that returns a single value
        System.out.println("Total number of characters in all names: " +
                friends.stream()
                        .mapToInt(name -> name.length())
                        .sum());


        //find the longest name
        final Optional<String> aLongName =
                friends.stream()
                        .reduce((name1, name2) ->
                                name1.length() >= name2.length() ? name1 : name2);

        System.out.println(aLongName);
//        aLongName.ifPresent(name ->
//                System.out.println(String.format("A longest name: %s", name)));


        //there are two kinds of reductions

        System.out.println( friends.stream()
                        .map(friend -> friend.length())     //intermediary step,  done lazily, returns a Stream<Integer>
                        .max(Comparator.<Integer>naturalOrder())); //terminal step, done eagerly, returns void



        System.out.println( friends.stream()
                .map(friend -> friend.length())     //intermediary step,  done lazily, returns a Stream<Integer>
                .allMatch(len -> len < 20)); //terminal step, done eagerly, returns void











    }   //end main






}
