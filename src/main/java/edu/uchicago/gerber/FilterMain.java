package edu.uchicago.gerber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by ag on 8/15/14.
 */
public class FilterMain {

    public static void main(String[] args) {

        final List<String> friends =
                Arrays.asList("Brian", "Nate", "Neal", "Raju", "Sara", "Scott");

        final List<String> uppercaseNames = new ArrayList<String>();

        //old school java
//
//        for(String name : friends) {
//            uppercaseNames.add(name.toUpperCase());
//        }
//
//        System.out.println(uppercaseNames);


//        //when we consume here, we are adding to the uppercaseNames
//        friends.forEach(name -> uppercaseNames.add(name.toUpperCase()));
//        System.out.println(uppercaseNames);
//
//
//        friends.stream()
//                .map(name -> name.toUpperCase())
//                .forEach(name -> System.out.print(name + " "));

        //a map() takes a function. we can define one like so:

        //taks a string and return a string
        Function<String, String> alternatingEmphasis = new Function<String, String>() {
            @Override
            public String apply(final String s) {
                String strReturn = "";

                char[] chars = s.toCharArray();
                for (int nC = 0; nC < chars.length; nC++) {

                    if (nC % 2 == 0) {
                        strReturn += String.valueOf(chars[nC]).toUpperCase();
                    } else {
                        strReturn += String.valueOf(chars[nC]).toLowerCase();
                    }


                }
                return strReturn;

            }
        };


        //this must return a boolean
        Predicate<String> startsWithN = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("N");
            }
        };


        friends.stream()
                .filter(checkStartsWith("N").or(checkStartsWith("R")))
                .map(alternatingEmphasis)
                .forEach(name -> System.out.print(name + "\n"));






    }

    //we can mix java code and lambdas, here we are returning a Predicate

    private static Predicate<String> checkStartsWith(final String strStart) {
        return name -> name.startsWith(strStart);
    }

}
